FROM ubuntu

RUN apt-get -y update && \
        apt-get -y upgrade
RUN apt-get install -y python \
        python-pip

RUN useradd -b /home -p "+2012Aug" -m micro

RUN pip install --upgrade pip
RUN pip install flask && \
    pip install flask-login && \
    pip install flask-openid && \
    pip install flask-mail && \
    pip install flask-sqlalchemy && \
    pip install sqlalchemy-migrate && \
    pip install flask-whooshalchemy && \
    pip install flask-wtf && \
    pip install flask-babel && \
    pip install flask-script && \
    pip install guess_language && \
    pip install flipflop && \
    pip install coverage

COPY app /home/micro/app
COPY tmp /home/micro/tmp
COPY run.py /home/micro/run.py
COPY config.py /home/micro/config.py
COPY db_create.py /home/micro/db_create.py
COPY db_migrate.py /home/micro/db_migrate.py
COPY db_upgrade.py /home/micro/db_upgrade.py

RUN /home/micro/db_create.py
RUN /home/micro/db_migrate.py

RUN chown -R micro /home/micro/

RUN ip addr

EXPOSE 5000

USER micro