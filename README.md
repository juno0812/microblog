# microblog
following https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world

## Building and Running
Build the Docker container with `build_container.sh`, and run the container along with the web app with `run_container.sh`.  To be quick, I usually chain these together with `&&`.