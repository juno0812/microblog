from flask_wtf import Form
from wtforms import StringField, SubmitField, PasswordField, BooleanField
from wtforms.validators import Required, Email, EqualTo
from models import User

class LoginForm(Form):
	email = StringField('email', validators=[Required(), Email()])
	password = PasswordField('password', validators=[Required()])
	remember_me = BooleanField('keep me logged in')

class RegistrationForm(Form):
	nickname = StringField('name', validators=[Required()])
	email = StringField('email', validators=[Required(), Email()])
	password = PasswordField('password', validators=[Required(), EqualTo('password2', message='passwords must match')])
	password2 = PasswordField('confirm password', validators=[Required()])

	def validate_email(self, field):
		if User.query.filter_by(email=field.data).first():
			raise ValidationError('email already registered')