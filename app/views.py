from flask import render_template, flash, redirect, url_for, request, g
from flask_login import login_user, logout_user, login_required, current_user
from models import User
from app import app, db
from .forms import LoginForm, RegistrationForm

@app.before_request
def before_request():
    g.user = current_user

# @app.after_request
# def after_request():
#     db.session.commit()

@app.route('/')
@app.route('/index')
def index():
    user = g.user
    # TODO: need to get the current user from the session and view here
    posts = [
    	{
    		'author': {'nickname': 'John'},
    		'body': 'Beautiful day in Portland!'
    	},
    	{
    		'author': {'nickname': 'Susan'},
    		'body': 'The movie was cool!'
    	}
    ]
    return render_template('index.html',
    					  title='Home',
    					  user=user,
    					  posts=posts)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if g.user is not None and g.user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('index'))
        flash('Invalid username or password.')
    return render_template('login.html', title='Sign In', form=form)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(nickname=form.nickname.data, email=form.email.data, password=form.password.data)
        db.session.add(user)
        db.session.commit()  # should go in after_app_request
        flash('you can now login.')
        return redirect(url_for('login'))
    return render_template('register.html', form=form)